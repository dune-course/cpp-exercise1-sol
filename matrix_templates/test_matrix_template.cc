/**
 *  Exercise 1 - Part I
 *  
 *  Main program
 */

#include "matrix_template.h"

int main()
{   
    // define a matrix A
    MatrixClass<double> A(4,1);
    
    for (int i=0;i<A.Rows();++i)
      for (int j=0;j<A.Cols();++j)
      A(i,j) = 1;
    
    /*
    for (int i=0;i<A.Rows();++i)  
      A(i,i) = 2.0;
    for (int i=0;i<A.Rows()-1;++i) 
      A(i+1,i) = A(i,i+1) = -1.0;
    */

    // print content of A
    std::cout << "A : " <<std::endl;
    A.Print();
  
    // define a matrix C with same content as A
    MatrixClass<double> C = A;
    std::cout << "C : " <<std::endl;
    C.Print();

    A = 2.*C;
    std::cout << "A = 2 * C" <<std::endl;
    A.Print();
    
    A = C*2.;
    std::cout << "A = C * 2." <<std::endl;
    A.Print();
    
    A = C + A + A + C;
    std::cout << "A = C + A + A + C" <<std::endl;
    A.Print();
    
    A.Resize(5,5);
    for (int i=0;i<A.Rows();++i)  
      A(i,i) = 2.0;
    for (int i=0;i<A.Rows()-1;++i) 
      A(i+1,i) = A(i,i+1) = -1.0;

    // Print A
    std::cout << "A :" <<std::endl;
    A.Print();

}
