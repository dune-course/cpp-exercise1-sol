#include "matrix_double.h"
#include <cstdlib>
#include <iomanip>
#include <iostream>

// Set number of matrix rows and columns and 
// initialize matrix elements with a given double value
void MatrixClass::Resize(int numRows, int numCols, const double &value)
{
  a_.resize(numRows);
  for (int i=0;i<a_.size();++i)
  {
    a_[i].resize(numCols);
    for (int j=0;j<a_[i].size();++j)
      a_[i][j]=value;
  }
  numRows_=numRows;
  numCols_=numCols;
}

// Access matrix element at position (i,j)
double &MatrixClass::operator()(int i, int j)
{
  if ((i<0)||(i>=numRows_))
  {
    std::cerr << "Illegal row index " << i;
    std::cerr << " valid range is (0:" << numRows_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  if ((j<0)||(j>=numCols_))
  {
    std::cerr << "Illegal column index " << j;
    std::cerr << " valid range is (0:" << numCols_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  return a_[i][j];
}

// Access matrix element at position (i,j)
double MatrixClass::operator()(int i,int j) const
{
  if ((i<0)||(i>=numRows_))
  {
    std::cerr << "Illegal row index " << i;
    std::cerr << " valid range is (0:" << numRows_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  if ((j<0)||(j>=numCols_))
  {
    std::cerr << "Illegal column index " << j;
    std::cerr << " valid range is (0:" << numCols_-1 << ")";
    std::cerr << std::endl;
    exit(EXIT_FAILURE);
  }
  return a_[i][j];
}

// Output matrix content
void MatrixClass::Print() const
{
  std::cout << "(" << numRows_ << "x";
  std::cout << numCols_ << ") matrix:" << std::endl;
  for (int i=0;i<numRows_;++i)
  {
    std::cout << std::setprecision(3);
    for (int j=0;j<numCols_;++j)
        std::cout << std::setw(5) << a_[i][j] << " ";
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

// Arithmetic functions 

// Multiplication by value x
MatrixClass &MatrixClass::operator*=(double x)
{
  for (int i=0;i<numRows_;++i)
    for (int j=0;j<numCols_;++j)
      a_[i][j]*=x;
      
  return *this;
}

// Addition
MatrixClass &MatrixClass::operator+=(const MatrixClass &x)
{
  if ((x.numRows_!=numRows_)||(x.numCols_!=numCols_))
  {
    std::cerr << "Dimensions of matrix a (" << numRows_
              << "x" << numCols_ << ") and matrix x (" 
              << numRows_ << "x" << numCols_ << ") do not match!";
    exit(EXIT_FAILURE);
  }
  for (int i=0;i<numRows_;++i)
    for (int j=0;j<x.numCols_;++j)
      a_[i][j]+=x(i,j);
  return *this;
}


// More arithmetic functions
MatrixClass operator*(const MatrixClass &a, double x)
{
  MatrixClass temp(a);
  temp *= x;
  return temp;
}

MatrixClass operator*(double x, const MatrixClass &a)
{
  MatrixClass temp(a);
  temp *= x;
  return temp;
}

//Concatenate
MatrixClass operator+(const MatrixClass &a,const MatrixClass &b)
{
  MatrixClass temp(a);
  temp += b;
  return temp;
}
