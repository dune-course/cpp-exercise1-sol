#include "matrix_double.h"
#include <iostream>
#include <cstdlib>

int main()
{   
    // Define a matrix A
    MatrixClass A(4,4);
    std::cout << "A : " <<std::endl;
    A.Print();

    /*
    // Define a vector
    for (int i=0;i<A.Rows();++i)  
      for (int j=0;j<A.Cols();++j) 
	A(i,j) = 1.0;
    */

    // Define a square tridiagonal matrix A
    for (int i=0;i<A.Rows();++i)  
      A(i,i) = 2.0;
    for (int i=0;i<A.Rows()-1;++i) 
      A(i+1,i) = A(i,i+1) = -1.0;

    // Define a matrix C of same content as A
    MatrixClass C(A);
    std::cout << "C : " <<std::endl;
    C.Print();
    
    A = 2*C;
    std::cout << "A = 2 * C" <<std::endl;
    A.Print();
    
    A = C*2.;
    std::cout << "A = C * 2." <<std::endl;
    A.Print();
    
    A = C+A;
    std::cout << "A = C + A" <<std::endl;
    A.Print();
    
    //Resize A
    A.Resize(5,5);
    for (int i=0;i<A.Rows();++i)  
      A(i,i) = 2.0;
    for (int i=0;i<A.Rows()-1;++i) 
      A(i+1,i) = A(i,i+1) = -1.0;

    // Print A
    std::cout << "A :" <<std::endl;
    A.Print();
 
}
